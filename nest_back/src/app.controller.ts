import { Body, Controller, Delete, Get, Param, Post, Put } from "@nestjs/common";
import { AppService } from './app.service';
import { CarDto } from './entities/car.dto';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getAllCar() {
    return this.appService.getAllCar();
  }

  @Get(':id')
  getCar(@Param('id') id: number) {
    return this.appService.getCar(id);
  }

  @Post()
  saveCar(@Body() body: CarDto) {
    return this.appService.saveCar(body);
  }

  @Put(':id')
  modifyCar(@Param('id') id: number, @Body() body: CarDto) {
    return this.appService.modifyCar(id, body);
  }

  @Delete(':id')
  removeCar(@Param('id') id: number) {
    return this.appService.removeCar(id);
  }
}
