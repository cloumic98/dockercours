export type CarDto = {
  brand: string;
  model: string;
  year: number;
  plate: string;
};
