import { Injectable } from '@nestjs/common';
import { Repository } from "typeorm";
import { InjectRepository } from '@nestjs/typeorm';
import { Car } from './entities/car.entity';
import { CarDto } from "./entities/car.dto";

@Injectable()
export class AppService {
  constructor(
    @InjectRepository(Car)
    private usersRepository: Repository<Car>,
  ) {}

  getAllCar() {
    return this.usersRepository.find();
  }

  getCar(id: number) {
    return this.usersRepository.findOneBy({ id: id });
  }

  saveCar(data: CarDto) {
    const car = this.usersRepository.create(data);

    return this.usersRepository.save(car);
  }

  modifyCar(id: number, data: CarDto) {
    return this.usersRepository.save({
      id: id,
      brand: data.brand,
      model: data.model,
      year: data.year,
      plate: data.plate,
    });
  }

  removeCar(id: number) {
    return this.usersRepository.delete(id);
  }
}
