import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Car } from './entities/car.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'mariadb',
      port: 3306,
      username: 'root',
      password: 'root',
      database: 'test',
      entities: [Car],
      synchronize: true,
    }),
    TypeOrmModule.forFeature([Car]),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
