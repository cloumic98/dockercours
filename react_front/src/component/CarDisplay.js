import axios from "axios";

export function CarDisplay({ car }) {

    const saveCar = () => {
        axios.put('http://localhost:3000/' + car.id, car)
    }

    return (
        <>
            <p>{car.id}</p>
            <p>{car.brand}</p>
            <p>{car.model}</p>
            <p>{car.year}</p>
            <p>{car.plate}</p>
            <button>Save</button><button>Delete</button>
        </>
    )
}