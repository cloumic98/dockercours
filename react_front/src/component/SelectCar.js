import {useEffect, useState} from "react";
import axios from "axios";
import {CarDisplay} from "./CarDisplay";

export function SelectCar(props) {
    const [cars, setCars] = useState([]);
    const [selectedCar, setSelectedCar] = useState(null);

    useEffect(() => {
        axios.get('http://localhost:3000').then((response) => {
            setCars(response.data)
        })
    }, [])

    const selection = (event) => {
        if(event.target.selectedIndex != 0) {
            setSelectedCar(cars[event.target.selectedIndex-1])
        }
    }

    return (
        <>
            <select onChange={selection} defaultValue={0}>
                <option>Select a car</option>
                {
                    cars.map((car, index) => (<option key={index + "-" + car.id} value={car.id}>{car.brand + " " + car.model}</option>))
                }
            </select>
            <br/>
            { selectedCar && (<CarDisplay car={selectedCar}></CarDisplay>)}
        </>
    )
}